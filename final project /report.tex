\documentclass{ctexart}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{xeCJK}
\usepackage{listings}

 \title{proj-math-soft}
 \author{hushmoon}
 \date{July 2023}

 \begin{document}
 \maketitle

 \section{Introduction}

 这是第一个使用有限元方法计算问题的例子。我们将求解简化版本的泊松方程，其中边界值为零，右手边为非零：

\[-\Delta u = f\quad \text{in}\Omega\]
\[ u = 0\quad \text{on}\partial\Omega\]
我们将在正方形区域 $\Omega = [-1,1]^2$ 上求解该方程，你已经学习过如何在步骤1和步骤2中生成网格。在这个程序中，我们仅考虑特殊情况 $f(x) = 1$，并在下一个教程程序（步骤4）中回顾如何实现更一般的情况。

如果你已经学习了有限元方法的基础知识，你会记得我们需要采取以下步骤，通过有限维近似来近似求解 $u$。具体而言，我们首先需要推导出上述方程的弱形式，通过从左边乘以一个测试函数 $\varphi$ 并在域 $\Omega$ 上进行积分来得到：

\[-\int_{\Omega} \varphi \Delta u = \int_{\Omega} \varphi f.\]

这可以通过积分换元得到：

\[\int_{\Omega} \nabla \varphi \cdot \nabla u - \int_{\partial \Omega} \varphi n \cdot \nabla u = \int_{\Omega} \varphi f.\]

测试函数 $\varphi$ 需要满足与边界条件相同的要求（在数学术语中，它需要来自我们寻找解的集合的切空间），因此在边界上 $\varphi = 0$，因此我们所寻找的弱形式为：

\[(\nabla \varphi, \nabla u) = (\varphi, f),\]

其中我们使用了常见的表示法 $(a,b) = \int_{\Omega} ab$。然后，该问题要求寻找一个函数 $u$，对于所有适当空间中的测试函数 $\varphi$，上述等式都成立（在这里是空间 $H^1$）。

当然，在计算机上一般情况下我们无法找到这样的函数，而是寻求一个近似解 $u_h(x) = \sum_j U_j \varphi_j(x)$，其中 $U_j$ 是需要确定的未知展开系数（该问题的“自由度”），$\varphi_i(x)$ 是我们将要使用的有限元形状函数。为了定义这些形状函数，我们需要以下内容：

\begin{itemize}
  \item 一个网格，用于定义形状函数。你已经在步骤1和步骤2中看到了如何生成和操作描述网格的对象。
  \item 描述我们想要在参考单元上使用的形状函数的有限元。在步骤2中，我们已经使用了 \texttt{FE\_Q<2>} 类型的对象，它表示通常的拉格朗日元素，通过插值在支撑点上定义形状函数。最简单的情况是 \texttt{FE\_Q<2>(1)}，它使用多项式次数为1。在二维中，这些通常被称为双线性，因为它们在参考单元的每个坐标上是线性的。（在一维中，它们是线性的，在三维中是三线性的；然而，在 \texttt{deal.II} 文档中，我们经常不做这个区分，而是简单地将这些函数都称为“线性”。）
  \item 一个 \texttt{DoFHandler} 对象，对网格上的所有自由度进行枚举，以基于有限元对象提供的参考单元描述为基础。你在步骤2中也已经看到了如何做到这一点。
  \item 一个映射，告诉我们如何从实际单元上的形状函数获得从参考单元上的有限元类定义的形状函数。默认情况下，除非你明确说明，\texttt{deal.II} 将使用（双、三）线性映射，因此在大多数情况下，你不必担心这一步骤。
\end{itemize}

通过这些步骤，我们现在有一组函数 $\varphi_i$，并且我们可以定义离散问题的弱形式：寻找一个函数 $u_h$，即寻找上述提到的展开系数 $U_j$，使得

\[(\nabla \varphi_i, \nabla u_h) = (\varphi_i, f), \quad i = 0,\ldots,N-1.\]

请注意，我们遵循从零开始计数的约定，与C和C++中常见的约定一致。这个方程可以重写为线性系统，如果你插入表示 $u_h(x) = \sum_j U_j \varphi_j(x)$，然后观察到

\[(\nabla \varphi_i, \nabla u_h) = (\nabla \varphi_i, \nabla [\sum_j U_j \varphi_j]) = \sum_j (\nabla \varphi_i, \nabla [U_j \varphi_j]) = \sum_j (\nabla \varphi_i, \nabla \varphi_j) U_j.\]

有了这个结果，问题就变成了：寻找一个向量 $U$，使得

\[AU = F,\]

其中矩阵 $A$ 和右手边 $F$ 定义为

\[A_{ij} = (\nabla \varphi_i, \nabla \varphi_j), \quad F_i = (\varphi_i, f).\]
\subsection{应该从左边还是从右边乘以一个测试函数？}
在我们继续描述如何计算这些量之前，请注意，如果我们将原方程从右边而不是从左边乘以一个测试函数，那么我们将得到一个形式为

\[ 
U^T A = F^T 
\]

的线性系统，其中 \(F^T\) 是一个行向量。通过转置这个系统，当然等价于解决

\[ 
A^T U = F 
\]

这在这里与上述相同，因为 \(A = A^T\) 。但一般情况下并非如此，为了避免任何混淆，经验表明，习惯上从左边而不是从右边乘以方程（常见于数学文献中），可以避免常见的错误类别，因为矩阵会自动正确且不需要在比较理论和实现时进行转置。请参考本教程中的第一个示例step-9，其中我们针对一个非对称的双线性形式，乘法是从右边还是从左边对结果会有影响。\\

\subsection{组装矩阵和右侧向量}

现在我们知道我们需要什么（即：持有矩阵和向量的对象，以及计算$A_{ij}$和$F_i$的方法），接下来我们可以看看如何实现：

矩阵$A$的对象类型是SparseMatrix，向量$U$和$F$的对象类型是Vector。下面的程序会展示用于解线性系统的类。
我们需要一种形成积分的方法。在有限元方法中，最常用的方法是使用数值积分，即通过对每个单元格上的一组数值积分点进行加权求和来代替积分。也就是说，我们首先将$\Omega$上的积分拆分为所有单元格上的积分：
\[
A_{ij}= (\nabla \varphi_i, \nabla \varphi_j) = \sum_{K\in \mathcal{T}} \int_K \nabla \varphi_i \cdot \nabla \varphi_j,
\]

\[
F_i = (\varphi_i, f) = \sum_{K\in \mathcal{T}} \int_K \varphi_i f,
\]
然后通过数值积分近似每个单元格的贡献：
\[
A_{ij}^{K} = \int_K \nabla \varphi_i \cdot \nabla \varphi_j \approx \sum_q \nabla \varphi_i(x_{q}^{K}) \cdot \nabla \varphi_j(x_{q}^{K}) w_{q}^{K},
\]

\[
F_{i}^{K} = \int_K \varphi_i f\approx \sum_q \varphi_i(x_{q}^{K})f(x_{q}^{K})w_{q}^{K},
\]
其中$\mathcal{T} \approx \Omega$是逼近域的三角剖分，$x_{Kq}$是第$q$个积分点在单元格$K$上的坐标，$w_{Kq}$是第$q$个积分权重。在进行数值积分时，需要满足一些条件，接下来我们将逐个讨论它们。
\begin{itemize}
    \item 
    首先，我们需要一种描述积分点$x_{Kq}$和权重$w_{Kq}$的方式。通常情况下，它们会通过类MappingQ1（或者如果您明确指定了，可以使用从Mapping派生出的其他类）从参考单元映射而来，就像形状函数一样。参考单元上的积分点和权重由继承自Quadrature基类的对象描述。通常，人们会选择一个积分公式（即一组点和权重），使得数值积分与矩阵中的积分完全相等；这是因为积分中的所有因子都是多项式函数，并且可以通过高斯积分公式来实现，QGauss类中实现了它。
    \item 
    然后，我们需要一些方法来计算单元格$K$上的$\varphi_i(x_{Kq})$。这就是FEValues类的作用：它接受一个有限元对象来描述参考单元上的$\varphi$，一个积分对象来描述积分点和权重，以及一个映射对象（或者隐式使用MappingQ1类），并在位于$K$上的积分点处提供形状函数的值和导数，以及进行积分所需的各种其他信息。
\end{itemize}
计算矩阵和右边作为所有单元的总和（然后是正交点的总和）的过程通常称为组装线性系统，或简称为组装，使用与流水线相关的词义，意思是 "将一组碎片、片段或元素组装在一起的行为"。

FEValues确实是组装过程中的核心类。您可以按以下方式查看它： FiniteElement和派生类描述形状函数，即无限维对象：函数在每一点都有值。出于理论原因，我们需要这样做，因为我们希望通过对函数的积分来进行分析。然而，对于计算机来说，这是一个非常困难的概念，因为计算机一般只能处理有限的信息量， 因此我们用对正交点的求和来代替积分，正交点是通过将参考单元上定义的点（正交对象）映射（映射 对象）到实际单元上的点而得到的。从本质上讲，我们将问题简化为只需要有限信息量的问题，即形状函数值和导数、正交权重、法向量等，这些信息只存在于有限的点上。FEValues类将这三个部分结合在一起，并提供特定单元$K$上的有限信息集。

值得注意的是，如果您只需在应用程序中创建这三个对象，并自己处理这些信息，也可以实现所有这些功能。然而，这样做既不会更简单（FEValues类提供的正是您实际需要的信息），也不会更快：FEValues类经过高度优化，只在每个单元格中计算您需要的特定信息；如果可以重复使用上一个单元格中的任何信息，那么它就会这样做，而且该类中有大量代码可以确保在任何有利的地方缓存信息。

最后要提到的是，在得到线性系统后，我们使用迭代求解器对其进行求解，然后进行后处理：我们使用DataOut类创建一个输出文件，然后可以使用常见的可视化程序对其进行可视化。

\subsection{求解线性系统}
对于有限元程序来说，这里的线性系统相对较小：矩阵大小为1089×1089，因为我们使用的网格是$32×32$，所以网格中有$33^2=1089$个顶点。在后面的教程中，矩阵大小在几万到几十万之间的情况并不少见，在deal.II基础上开发的ASPECT等代码中，我们经常求解超过一亿个方程的问题（尽管使用的是并行计算机）。无论如何，即使是这里的小系统，矩阵也比本科生或大多数研究生课程中通常遇到的大得多，因此问题来了，我们如何解决这样的线性系统。

我们通常学习的第一种求解线性系统的方法是高斯消元法。这种方法的问题在于它需要的运算次数与$N^3$成正比，其中$N$是线性系统中的方程或未知数的个数--更具体地说，运算次数是$\frac{2}{3}N^3$，或多或少。在$N=1089$的情况下，这意味着我们需要进行大约8.61亿次运算。这是一个相当可行的数字，现代处理器只需不到0.1秒的时间即可完成。但很明显，这并不适合扩展： 如果我们的线性方程组中的方程数量是它的20倍（也就是未知数数量是它的20倍），那么我们就需要花费1000-10000秒，也就是一个小时的时间。如果线性方程组的数量再增加10倍，那么很明显，我们就无法在一台计算机上解决这个问题了。

如果我们意识到矩阵中只有相对较少的条目为非零，即矩阵是稀疏的，就可以在一定程度上解决这个问题。高斯消元法的变种可以利用这一点，使求解过程大大加快；我们将在步骤29中首次使用这样一种方法--在SparseDirectUMFPACK类中实现，以及之后的其他几种方法。高斯消除的这些变化可能会使我们的问题规模达到10万或20万的数量级，但不会超过这个数量级。

取而代之的是1952年提出的共轭梯度法，简称 "CG"。CG是一种 "迭代 "求解方法，它形成的向量序列收敛于精确解；事实上，在没有舍入误差的情况下，经过$N$次这样的迭代后，如果矩阵是对称和正定的，它就能找到精确解。该方法最初是作为精确求解线性系统的另一种方法而开发的，就像高斯消元法一样。但是，当计算机变得足够强大，可以解决高斯消元法已经不能很好解决的问题时（20世纪80年代的某个时候），CG被重新发现，因为人们意识到它非常适合大型稀疏系统，就像我们从有限元法中得到的系统一样。这是因为：(i)它计算的向量收敛于精确解，因此，只要我们对合理的近似值感到满意，实际上就不必进行$N$次迭代来找到精确解；(ii)它只需要矩阵-向量乘积，这对稀疏矩阵非常有用，因为稀疏矩阵的定义是只有$(N)$个条目，因此矩阵-向量乘积只需$(N)$次运算即可完成，而对密集矩阵进行同样的运算则需要$N^2$次运算。因此，我们希望用最多$(N^2)$次运算来求解线性系统，而且在许多情况下会大大减少。

因此，有限元代码几乎总是使用CG等迭代求解器来求解线性系统，我们在本代码中也将这样做。(我们注意到，CG方法仅适用于对称和正定矩阵；对于其他方程，矩阵可能不具备这些特性，我们将不得不使用其他迭代求解器的变体，如BiCGStab或GMRES，它们适用于更一般的矩阵.)

这些迭代求解器的一个重要组成部分是我们指定解线性系统所允许的容差，本质上是我们对近似解的误差接受程度的说明。对于线性系统 $Ax=b$ 的精确解 $x$ 和得到的近似解 $\tilde{x} $，误差定义为 $\|x−\tilde{x} \|$，但由于不知道精确解 $x$，我们无法计算该量。因此，我们通常将残差视为可计算的度量，其定义为 $\|b−A\tilde{x} \|=\|A(x−\tilde{x} )\|$。然后，我们让迭代求解器计算出越来越精确的解 $\tilde{x} $，直到 $\|b−A\tilde{x} \|≤\tau $。一个实际问题是，参数$\tau $应该取什么值。在大多数应用中，设置

$\tau =10^{-6}\|b\|$

是一个合理的选择。我们将 $\tau$ 与 $b$ 的大小（范数）成比例，以确保我们对解的精确度的期望相对于解的大小而言。这是有道理的：如果我们把右边的向量 $b$ 放大十倍，那么方程 $Ax=b$ 的解 $x$ 也会放大十倍，$\tilde{x} $ 也会如此；我们希望 $\tilde{x} $ 中的准确数字与之前相同，这意味着当残差 $\|b−A\tilde{x} \|$ 放大十倍时我们也应该终止迭代，而如果我们使 $\tau$ 与 $\|b\|$ 成比例，正好能够实现这一点。

所有这些将在程序中的 Step3::solve() 函数中实现。正如您将看到的，使用 deal.II 设置线性求解器非常简单：整个函数只有三行代码。

\subsection{关于项目实施}
虽然这是用有限元方法求解的最简单方程，但这个程序显示了大多数有限元程序的基本结构，也是几乎所有后续程序的模板。具体来说，本程序的主类如下：
\begin{lstlisting}
class Step3
{
  public:
    Step3 ();
    void run ();
 
  private:
    void make_grid ();
    void setup_system ();
    void assemble_system ();
    void solve ();
    void output_results () const;
 
    Triangulation<2>     triangulation;
    FE_Q<2>              fe;
    DoFHandler<2>        dof_handler;
 
    SparsityPattern      sparsity_pattern;
    SparseMatrix<double> system_matrix;
    Vector<double>       solution;
    Vector<double>       system_rhs;
};
\end{lstlisting}

这遵循了面向对象编程的数据封装原则，即我们尽最大努力将该类的几乎所有内部细节隐藏在外部无法访问的私有成员中。

让我们从成员变量开始:我们需要一个Triangulation对象和一个DoF-Handler对象，以及一个描述我们希望使用的形状函数的有限元对象。第二组对象与线性代数有关：系统矩阵和右边以及解向量，还有一个描述矩阵稀疏性模式的对象。这就是该类所需要的全部内容（也是任何静态PDE求解器所需要的基本内容），并且需要在整个程序中保持不变。与此相反，我们在装配时需要的FEValues对象只在整个装配过程中需要，因此我们在函数中将其创建为局部对象，并在函数结束时将其销毁。

其次，让我们看看成员函数。这些函数已经构成了几乎所有后续教程程序都将使用的通用结构：
\begin{itemize}
\item make\_grid():  这是一个预处理函数。顾名思义，它设置了存储三角剖分的对象。在后面的示例中，它还可以处理边界条件、几何图形等。
\item setup\_system()： 该函数用于设置解决问题所需的所有其他数据结构。特别是，它将初始化DoFHandler对象，并正确设置与线性代数有关的各种对象的大小。这个函数通常与上面的预处理函数分开，因为在一个与时间相关的程序中，每当网格进行自适应细化时，至少每隔几个时间步就会调用这个函数（我们将在第六步中了解如何进行细化）。另一方面，在上述预处理函数中设置网格本身只在程序开始时进行一次，因此将其分离到单独的函数中。
\item assemble\_system()： 这就是计算矩阵和右边内容的地方，在上面的介绍中已经详细讨论过。由于对该线性系统的处理在概念上与计算其条目截然不同，我们将其从下面的函数中分离出来。
\item solve()： 这是计算线性系统$AU=F$的解$U$的函数。在当前程序中，这是一个简单的任务，因为矩阵非常简单，但当问题不再那么微不足道时，它将成为程序大小的重要组成部分（例如，当你对函数库有了更多了解后，请参见步骤-20、步骤-22或步骤-31）。
\item output\_results()： 最后，当您计算出一个解后，您可能想对它做一些事情。例如，您可能希望以可视化的格式输出，或者您可能希望计算您感兴趣的量，例如，热交换器中的热通量、机翼的空气摩擦系数、最大桥梁载荷，或者仅仅是数值解在某一点的值。因此，该函数用于对求解结果进行后处理。
\end{itemize}
所有这些都由一个公共函数（构造函数除外），即run()函数来完成。从创建该类型对象的地方调用该函数，并按正确顺序调用所有其他函数。将这一操作封装到run()函数中，而不是从main()函数调用所有其他函数，可以确保您可以改变该类中关注点分离的实现方式。例如，如果其中一个函数变得过大，您可以将其拆分为两个函数，而您唯一需要关注的地方就是这个类内部，而不是其他任何地方。

如上所述，您将在下面的许多教程程序中再次看到这种一般结构--有时函数名称的拼写会有变化，但基本上是按照这种顺序分离功能的。
\subsection{关于类型的说明}
deal.II通过命名空间类型中的别名定义了许多积分类型。(在前面的句子中，"积分"一词是作为形容词使用的，与名词 "整数 "相对应。它不应与表示曲线或曲面下的面积或体积的名词 "积分 "相混淆。形容词 "integral "在C++世界中被广泛使用，例如 "integral type"（积分类型）、"integral constant"（积分常数）等）。特别地，在这个程序中，你会在几个地方看到types::global\_dof\_index：一个整数类型，用来表示一个自由度的全局索引，也就是在三角剖分上定义的DoFHandler对象中特定自由度的索引（与特定单元格中特定自由度的索引相反）。对于当前程序（以及几乎所有的教程程序），您将会有几千到几百万个全局未知数（对于Q1元素，您将会在2D中的每个单元格上有4个局部未知数，在3D中则有8个）。因此，无符号int是一种可以存储足够大的全局DoF指数的数据类型，因为它可以存储0到略高于40亿之间的数字（在大多数系统中，整数是32位的）。事实上，这就是 types::global\_dof\_index 的用途。

那么，为什么不直接使用无符号int呢？deal.II在7.3版本之前一直这样做。然而，deal.II支持非常大的计算（通过step-40讨论的框架），当分布在几千个处理器上时，可能有超过40亿个未知数。因此，在某些情况下，无符号int不够大，我们需要64位无符号积分类型。为了实现这一点，我们引入了 types::global\_dof\_index，它默认定义为简单的无符号 int，但如果需要，也可以通过在配置时传递特定标志（参见 ReadMe 文件），将其定义为无符号 long int。

这涵盖了技术方面。但是还有一个文档目的：在库中的任何地方，以及基于库的代码中，如果你看到一个地方使用了数据类型 types::global\_dof\_index，你就会立即知道被引用的量实际上是一个全局的 dof 索引。如果我们只是使用无符号int（也可能是局部索引、边界指示器、材料id等），就不会有这样的含义。立即知道一个变量指代的是什么也有助于避免错误：如果你看到一个类型为 types::global\_dof\_index 的对象被赋值给类型为 types::subdomain\_id 的变量，尽管它们都是由无符号整数表示的，编译器也不会因此抱怨，那么很明显这一定是个错误。

从更实际的角度来看，这种类型的存在意味着在装配过程中，我们需要创建一个4×4矩阵（二维，使用Q1元素）来表示当前单元的贡献，然后我们需要将该矩阵的元素添加到全局（系统）矩阵的相应元素中。为此，我们需要获取当前单元局部自由度的全局指数，为此我们将始终使用以下代码：
\begin{lstlisting}
cell->get_dof_indices (local_dof_indices);
\end{lstlisting}
其中 local\_dof\_indices 声明为
\begin{lstlisting}
std::vector<types::global_dof_index> local_dof_indices 
      (fe.n_dofs_per_cell());
\end{lstlisting}
这个变量的名字可能有点名不副实--它的意思是 "当前单元格中局部定义的自由度的全局指数"--但是在整个库中，包含这些信息的变量都被统一命名为 "全局指数"。

\section{Description For step-3}
step-3主要在计算的是二维Laplace方程的有限元问题，step-3.cc程序主要调用了deal.II库进行有限元计算。
\\我们先看到Step3类的几个成员函数：
\begin{itemize}
  \item 构造函数Step3()用于初始化有限元对象和自由度处理器。
  \item make\_grid()函数生成用于计算的网格（三角化）。
  \item setup\_system()函数分配自由度，设置系统矩阵的稀疏模式，并初始化解向量和右手边向量。
  \item assemble\_system()函数通过遍历单元格和积分点，计算每个单元格的贡献，并将其添加到全局矩阵和向量中，从而组装系统矩阵和右手边向量。
  \item solve()函数使用共轭梯度法求解线性系统。
  \item output\_results()函数以GNUPLOT格式将解写入文件。
  \item run()函数按照适当的顺序调用上述函数来执行整个计算过程。
\end{itemize}
该类还有几个成员变量，包括三角化对象、有限元对象、自由度处理器、稀疏模式、系统矩阵、解向量和右手边向量。\\

通过对step-3.cc函数的build，最终生成了可执行文件，运行可执行文件后，产生了solution.gpl文件，里面存储了求解出来的所有数据。此时再调用gnuplot来对solution.gpl文件进行处理，生成solution.eps文件，里面绘制了solution.gpl所对应的图像.\\

solution.eps图像在下一页显示
\begin{figure}
    \centering
    \includegraphics{step-3.eps}
\end{figure}

\end{document}

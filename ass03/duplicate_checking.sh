#!/bin/bash
#math-soft  2023 ZJU  
#yuchenyu

# 获取当前脚本所在目录\并进入所在目录
script_dir=$(dirname "$0")
cd "$script_dir" || exit

# 比较两个文件的区别行数的function
compare_files() {
    file1="$1"
    file2="$2"
    
    file1_lines=$(wc -l < "$file1")
    file2_lines=$(wc -l < "$file2")
    
    diff_lines=$(diff "$file1" "$file2" | grep "^<" | wc -l)
    
    echo "$file1 $file2 $diff_lines $file1_lines $file2_lines"
}

# 比较目录下所有文件的区别行数的function
compare_directory() {
    directory="$1"
    
    # 遍历
    files=("$directory"/*)
    num_files=${#files[@]}
    if [ $num_files -eq 1 ] || [ $num_files -eq 2 ]; then
    echo "无重复文件"
    fi
    for ((i=0; i < num_files-1; i++)); do
        for ((j=i+1; j < num_files; j++)); do
            compare_files "${files[i]}" "${files[j]}"
        done
    done
}

# 获取目录路径并比较
directory=$(pwd)

compare_directory "$directory"

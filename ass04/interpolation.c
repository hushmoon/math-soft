#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_spline.h>

/*define一个数据集来保存程序运行后的数据*/
#define DATA_FILE "interp.dat"

int main(void)
{
    int i;
    double xi, yi, x[10], y[10];
    printf("#m=0,S=17\n");

    FILE *file =fopen(DATA_FILE,"w");
    if (file == NULL)
    {
            printf("无法打开文件\n");
            return 1;
    }
    
    for (i = 0; i < 10; i++)
    {
        x[i] = i + 0.5 * sin(i);
        y[i] = i + cos(i * i) + 3.220102320;
        printf("%g %g\n", x[i], y[i]);
    }
    
    printf("#m=1,S=0\n");
    gsl_interp_accel *acc = gsl_interp_accel_alloc();
    gsl_spline *spline = gsl_spline_alloc(gsl_interp_cspline, 10);
    gsl_spline_init(spline, x, y, 10);
    for (xi = x[0]; xi < x[9]; xi += 0.01)
    {
        yi = gsl_spline_eval(spline, xi, acc);
        fprintf(file,"%g %g\n",xi,yi);
        printf("%g %g\n", xi, yi);
    }
    gsl_spline_free(spline);
    gsl_interp_accel_free(acc);

    fclose(file);
    system("graph -T ps < interp.dat > interp.eps");
    return 0;
}
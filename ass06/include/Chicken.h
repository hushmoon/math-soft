#include<iostream>
#ifndef CHICKEN_H
#define CHICKEN_H

class Chicken
{
private:
    int age;
    int ID;
    double weight;

public:
    Chicken(int _a, int _I, double _w) : age(_a),ID(_I),weight(_w) {};

    ~Chicken() {
	std::cout << "Chicken dinner" << std::endl;
    };

    void sing () {
	std::cout << "Winner winner chicken dinner" << std::endl;
    };
};

#endif
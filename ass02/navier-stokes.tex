\documentclass{ctexart}
\usepackage{xltxtra} % 让 xelatex 支持UTF
\usepackage[margin=1.25in]{geometry}
\usepackage{graphicx} % 图形库
\graphicspath{{./figure/}}
\usepackage{amsmath} % AMS 的数学符号
\usepackage{amsfonts}

\title{Homework ass02}

\author{yuchenyu}

\begin{document}

\maketitle
\section{不可压缩纳维-斯托克斯方程}

二维不可压缩流体的流场完全由速度向量 $\mathbf{q} = (u(x, y), v(x, y)) \in \mathbb{R}^2$ 和压力 $p(x, y) \in \mathbb{R}$ 描述。这些函数是以下守恒定律的解（参见，例如，Hirsch, 1988）：

\begin{itemize}
    \item 质量守恒方程：
    \[
    \text{div}(\mathbf{q}) = 0 \quad \text{(12.1)}
    \]
    或者，使用显式的散度形式表示：
    \[
    \frac{\partial u}{\partial x} + \frac{\partial v}{\partial y} = 0 \quad \text{(12.2)}
    \]

    \item 动量守恒方程的紧凑形式：
    \[
    \frac{\partial \mathbf{q}}{\partial t} + \text{div}(\mathbf{q} \otimes \mathbf{q}) = -Gp + \frac{1}{Re} \Delta \mathbf{q} \quad \text{(12.3)}
    \]
    或者，显式形式为：
    \[
    \begin{cases}
        \frac{\partial u}{\partial t} + \frac{\partial u^2}{\partial x} + \frac{\partial uv}{\partial y} = -\frac{\partial p}{\partial x} + \frac{1}{Re} \left(\frac{\partial^2 u}{\partial x^2} + \frac{\partial^2 u}{\partial y^2}\right), \\
        \frac{\partial v}{\partial t} + \frac{\partial uv}{\partial x} + \frac{\partial v^2}{\partial y} = -\frac{\partial p}{\partial y} + \frac{1}{Re} \left(\frac{\partial^2 v}{\partial x^2} + \frac{\partial^2 v}{\partial y^2}\right).
    \end{cases}
    \quad \text{(12.4)}
    \]
\end{itemize}
前述方程用无量纲形式书写，使用以下缩放变量：

\[ x = \frac{x^*}{L}, \quad y = \frac{y^*}{L}, \quad u = \frac{u^*}{V_0}, \quad v = \frac{v^*}{V_0}, \quad t = \frac{t^*}{L/V_0}, \quad p = \frac{p^*}{\rho_0V_0^2}, \quad \text{(12.5)} \]

其中上标（*）表示以物理单位测量的变量。常数 $L$ 和 $V_0$ 分别代表模拟流体的参考长度和速度。维数无量纲数 $Re$ 称为雷诺数，用于量化流体中惯性（或对流）项和黏性（或扩散）项的相对重要性：

\[ Re = \frac{V_0L}{\nu}, \quad \text{(12.6)} \]

其中 $\nu$ 是流体的运动黏度。

总结起来，本项目中将通过数值方法求解由方程 (12.2) 和 (12.4) 定义的无量纲纳维-斯托克斯（Navier-Stokes）偏微分方程组。初始条件（在 $t = 0$ 时刻）和边界条件将在后续部分中讨论。

\section{计算域、交错网格和边界条件}

通过考虑一个矩形域 $L_x \times L_y$（见图12.1），并在整个域内使用周期性边界条件，可以大大简化纳维-斯托克斯方程的数值求解。速度 $q(x, y)$ 和压力 $p(x, y)$ 的周期性可以用以下数学表达式表示：

\[ q(0, y) = q(L_x, y), \quad p(0, y) = p(L_x, y), \quad \forall y \in [0, L_y], \quad (12.21) \]
\[ q(x, 0) = q(x, L_y), \quad p(x, 0) = p(x, L_y), \quad \forall x \in [0, L_x]. \quad (12.22) \]

我们在域内使用矩形且均匀的二维网格，在该网格上分布着计算点，这样可以方便地进行数值计算。由于不同变量在我们的方法中并不共享同一个网格，因此我们首先定义一个主网格（参见图1），沿着 x 方向取 $n_x$ 个计算点，沿着 y 方向取 $n_y$ 个计算点：

\begin{figure}[h]
    \centering
    \includegraphics[width=0.85\textwidth]{figure/figure.png}
    \caption{计算域，交错网格和边界条件.}
    \label{fig:enter-label}
\end{figure}

\[ x_c(i) = (i - 1) \delta x, \quad \delta x = \frac{L_x}{n_x - 1}, \quad i = 1, \ldots, n_x, \quad (12.23) \]
\[ y_c(j) = (j - 1) \delta y, \quad \delta y = \frac{L_y}{n_y - 1}, \quad j = 1, \ldots, n_y. \quad (12.24) \]

次要网格由主网格单元的中心点定义：

\[ x_m(i) = (i - 1/2) \delta x, \quad i = 1, \ldots, n_{xm}, \quad (12.25) \]
\[ y_m(j) = (j - 1/2) \delta y, \quad j = 1, \ldots, n_{ym}, \quad (12.26) \]

其中我们使用简写符号 $n_{xm} = n_x - 1$，$n_{ym} = n_y - 1$。在定义为矩形区域 $[x_c(i), x_c(i + 1)] \times [y_c(j), y_c(j + 1)]$ 的计算单元内，未知变量 $u$、$v$ 和 $p$ 将被计算为在不同空间位置的近似解：
\begin{itemize}
    \item  $u(i, j) \approx u(x_c(i), y_m(j))$（单元的西侧面），
    \item  $v(i, j) \approx v(x_m(i), y_c(j))$（单元的南侧面），
    \item  $p(i, j) \approx p(x_m(i), y_m(j))$（单元的中心）。
\end{itemize}
这种变量的交错排列具有压力和速度之间强烈的耦合优势。它还有助于避免在同一网格点上计算所有变量（称为共位排列）时出现的稳定性和收敛性问题（请参阅本章末尾的参考文献）\\
本文所引用的参考文献在本章末尾列出\cite{book1}。


\bibliographystyle{plain}
\bibliography{references}


\end{document}
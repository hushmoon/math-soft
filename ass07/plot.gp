set terminal pdf
set output "plot.pdf"

datafile = 'dimension.txt'
x = system(sprintf("awk '/^[0-9]+$/ {print; exit}' %s", datafile))
y = system(sprintf("awk '/^[0-9]+$/{getline;print} ' %s", datafile))
z = system(sprintf("awk '/^[0-9]+$/{getline;getline;print} ' %s", datafile))

set view equal xyz
set xrange [-10:10]
set yrange [-10:10]
set zrange [-10:10]

set parametric
set urange [0:2*pi]
set vrange [0:pi]
set isosamples 20, 10

splot 15 * cos(u) * sin(v), 20 * sin(u) * sin(v), 25 * cos(v) with lines lw 2 lc rgb "blue"
